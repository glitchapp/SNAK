function loadcontrollermappings()
	if love.filesystem.getInfo("lib/gamecontrollerdb.txt") then
		love.joystick.loadGamepadMappings("lib/gamecontrollerdb.txt")
	end
	lastgamepadtouchedlevel="none"	-- this allows the gamepad to vibrate in first init (check line 68 of game/mainfunctions/gamepad/gamepadRadialMenu.lua where the condition is checked)
	
	movementTimer = 0
	movementDelay = 0.1  -- Adjust this value to control movement speed (in seconds)

	axisMouseAcceleration=10		-- acceleration value if thumbstick is moved

	--loadGamePadFiles()
	
	
end

	-- Initialize variables for the updateButtonHeld(dt) function
	local buttonAHeldTime = 0
	local vibrationDelay = 0.75 -- Delay in seconds before vibration starts
	local lastVibrationTime = -vibrationDelay
	local vibrationInterval = 0.5 -- Vibration interval in seconds
	
	--joysticks = love.joystick.getJoysticks()
	--joystick = joysticks[1]


function updateButtonHeld(dt)

    if joystick then
		if joystick:isGamepadDown("a") then
			buttonAHeldTime = buttonAHeldTime + dt

			-- Check if it's time to start vibrating
			if buttonAHeldTime >= vibrationDelay then
				-- Check if it's time to vibrate
				if love.timer.getTime() - lastVibrationTime >= vibrationInterval then
					joystick:setVibration(0.2, 0.2, 0.1)
					lastVibrationTime = love.timer.getTime()
				end
			end
		else
			buttonAHeldTime = 0
		end
	
		if buttonAHeldTime > 3 and (languagehaschanged or musichaschanged or skinhaschanged or optionsmenupressed) then
			print("Saving changes")
			savemygame()
			languagehaschanged = false
			musichaschanged = false
			skinhaschanged = false
			optionsmenupressed = false
			if gamestatus=="gameplusoptions" then palette=1 end
			love.timer.sleep(0.3)
		end
    end

end





-- Function to update joystick axis and control emulated mouse movement
function updategetjoystickaxis(dt)
	gamepadTimer=gamepadTimer+dt
	 -- Check if a joystick is connected
	 if joystick ~= nil then
          -- Get values of left joystick axes
        leftxaxis = joystick:getGamepadAxis("leftx")
        leftyaxis = joystick:getGamepadAxis("lefty")
          
          -- Get values of right joystick axes
        rightxaxis = joystick:getGamepadAxis("rightx")
        rightyaxis = joystick:getGamepadAxis("righty")
        
          -- Get values of triggers
        tleft = joystick:getGamepadAxis('triggerleft')
        tright = joystick:getGamepadAxis('triggerright')

          -- Check if joystick is moved enough to register
        if leftxaxis>0.2 or leftxaxis<-0.2 then 
		
					if leftxaxis>0.2  then -- do nothing
				elseif leftxaxis<-0.2  then -- do nothing
				else
				end
				
		end
		
		if leftyaxis>0.2 or leftyaxis<-0.2 then
		-- Update emulated mouse y-coordinate
			
					if leftyaxis>0.4 then -- do nothing
				elseif leftyaxis<-0.2 then -- do nothing
				end
		end
 					
 
			-- activate zoom if right trigger is pressed
			if tright>0.2 then
				--zoomtriggered=true
		elseif tright<0.2 then
				--zoomtriggered=false
		end
   end
   
end




function isjoystickbeingpressed(joystick,button)
	if joystick and joystick:isGamepadDown("a") then
			if (mainstate:on("play")) then
				--playstate:keypressed(key)
				
				if joystick and joystick:isGamepadDown("a") then
					--play.launchBall(true)
					ControllerA=true
				else
					ControllerA=false
				end
			elseif (mainstate:on("splash")) then
				splash:keypressed(key)
			elseif (mainstate:on("menu")) then
				menu:keypressed(key)
			end
	elseif joystick and joystick:isGamepadDown("b") then
			mainstate:set("menu")
	elseif joystick and joystick:isGamepadDown("start") then
			StateManager.switch('pause_state')
			love.timer.sleep(0.3)
		
	end
end

--[[
-- Callback function when a gamepad button is pressed
function PlayStateisjoystickbeingpressed(joystick,button)
  	if joystick and joystick:isGamepadDown("leftshoulder") and (not play.tilt) then
            aplay(sounds.leftFlipper)
            pinball:moveLeftFlippers()
            print("test")
  	
  	end
end
--]]

--[[
-- Callback function when a gamepad button is pressed
function love.gamepadpressed(joystick,button)
  	if joystick and joystick:isGamepadDown("a") then
	
				if (self.state:on("main")) then  self:menuAction()
			elseif (self.state:on("config")) then  self:menuAction()
			elseif (self.state:on("about")) then   about:forward()
            elseif (self.state:on("scores")) then  self.state:set("main")
			end
	end
end
--]]
