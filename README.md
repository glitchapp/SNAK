# SNAK (forked from https://github.com/Keyslam/SNAK)

<img src="screenshot.webp" width=70% height=70%>

A great snake / sokoban puzzle game written by Justin van der Leij.  repository: https://github.com/Keyslam/SNAK

the game belongs to LÖVE jam 2020 and more info about it can be found here: https://keyslam.com/projects/snak/

I added gamepad support (you can control the game with thumbstick) and I replaced some assets and added sound effects and added a music track by Alex McCulloch Aka: "Pro Sensory" which can be found here: https://opengameart.org/content/rumble-jungle

MIT License

Copyright (c) 2020 Justin van der Leij
