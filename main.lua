currentLevel = 1

require ("ErrorHandling")

local Game = require("src.states.game")
local Gamestate = require("lib.gamestate")
local Timer = require("lib.timer")
	
	joystick = love.joystick.getJoysticks()[1]
	require ("gamepadinput")
	loadcontrollermappings()
	gamepadTimer=0
  
  leftxaxis,leftyaxis= 0,0
  
function love.load()
	Gamestate.registerEvents()
	Gamestate.switch(Game)
end

function love.update(dt)
	Timer.update(dt)
	
	
	updategetjoystickaxis(dt)
		
end

function love.keypressed(key)
	if key == 'escape' then love.event.quit() end
	if key == 'r' and love.keyboard.isDown 'lctrl' then
		love.event.quit 'restart'
	end
end
